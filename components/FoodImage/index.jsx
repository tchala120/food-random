import React from "react";
import PropTypes from "prop-types";

const FoodImage = ({ foodImage, foodName }) => (foodImage && foodName ? (
  <div className="food-random-container">
    <img className="food-image" src={foodImage} alt={foodName} />
    <strong className="food-name">{foodName}</strong>
  </div>
) : (
  <p className="loading-text">Finding dish for you...</p>
));

FoodImage.propTypes = {
  foodImage: PropTypes.string,
  foodName: PropTypes.string,
};

FoodImage.defaultProps = {
  foodImage: "",
  foodName: "",
};

export default FoodImage;
