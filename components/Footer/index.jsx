import React from "react";
import { CopyrightCircleOutlined } from "@ant-design/icons";

const Footer = () => (
  <footer>
    <div className="container footer-wrapper">
      <CopyrightCircleOutlined />
      {" "}
      <small className="ml-3 mb-0">Foodom</small>
    </div>
  </footer>
);

export default Footer;
