import React from "react";
import Head from "next/head";
import PropTypes from "prop-types";
import "../../styles/style.scss";

const Header = ({ title, description }) => (
  <Head>
    <title>{title ? `Foodom | ${title}` : "Foodom | Random Meal"}</title>
    <meta
      name="description"
      content={description || "Hello World Description"}
    />
    <link
      rel="stylesheet"
      href="//cdnjs.cloudflare.com/ajax/libs/antd/2.9.3/antd.min.css"
    />
    <link rel="icon" href="/foodom.png" />
  </Head>
);

Header.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
};

Header.defaultProps = {
  title: "",
  description: "",
};

export default Header;
