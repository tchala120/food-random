import React from "react";
import PropTypes from "prop-types";
import Header from "./Header";

const AppLayout = ({ title, description, children }) => (
  <>
    <Header title={title} description={description} />
    {children}
  </>
);

AppLayout.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  children: PropTypes.node,
};

AppLayout.defaultProps = {
  title: "",
  description: "",
  children: "",
};

export default AppLayout;
