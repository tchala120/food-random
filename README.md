## Foodom

Install Dependencies

```bash
npm run dev
# or
yarn dev
```

Run the Development Server

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
